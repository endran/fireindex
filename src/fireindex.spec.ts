import * as admin from 'firebase-admin';
import {FireIndex} from './fireindex';

export class JestMock<T> {
  fn: T | {[p: string]: jest.Mock<any>} | any = {};
  typed = (this.fn as any) as T;

  constructor(...methodNames: string[]) {
    methodNames.forEach((name) => (this.fn[name] = jest.fn()));
  }
}

export const testResolve = (value) => new Promise((resolve) => resolve(value));
export const testReject = (value) => new Promise((_, reject) => reject(value));

describe('FireIndex', () => {
  let adminMock: JestMock<any>;

  beforeEach(() => {
    adminMock = new JestMock('doc', 'set', 'get');
    adminMock.fn.doc.mockReturnValue(adminMock.fn);
  });

  describe('static create', () => {
    test('increments count', async () => {
      await FireIndex.create('TEST/PATH/FOR/CREATE', adminMock.typed);

      expect(adminMock.fn.doc).toHaveBeenCalledWith('fireIndex/TEST__PATH__FOR');
      expect(adminMock.fn.set).toHaveBeenCalledWith({count: admin.firestore.FieldValue.increment(1)}, {merge: true});
    });
  });

  describe('static delete', () => {
    test('decrements count', async () => {
      await FireIndex.delete('TEST/PATH/FOR/DELETE', adminMock.typed);

      expect(adminMock.fn.doc).toHaveBeenCalledWith('fireIndex/TEST__PATH__FOR');
      expect(adminMock.fn.set).toHaveBeenCalledWith({count: admin.firestore.FieldValue.increment(-1)}, {merge: true});
    });
  });

  describe('static count', () => {
    test('returns count', async () => {
      const getMock = new JestMock('data');
      getMock.fn.exists = true;
      getMock.fn.data.mockReturnValue({count: 111});
      adminMock.fn.get.mockReturnValue(getMock.typed);

      const actual = await FireIndex.count('TEST/PATH/FOR/DELETE', adminMock.typed);

      expect(adminMock.fn.doc).toHaveBeenCalledWith('fireIndex/TEST__PATH__FOR');
      expect(adminMock.fn.get).toHaveBeenCalled();
      expect(actual).toEqual(111);
    });

    test('returns 0 when cannot be found', async () => {
      const getMock = new JestMock('data');
      getMock.fn.exists = false;
      adminMock.fn.get.mockReturnValue(testResolve(getMock.typed));

      const actual = await FireIndex.count('TEST/PATH/FOR/DELETE', adminMock.typed);

      expect(adminMock.fn.doc).toHaveBeenCalledWith('fireIndex/TEST__PATH__FOR');
      expect(actual).toEqual(0);
    });
  });
});
