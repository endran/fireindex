import * as admin from 'firebase-admin';

export class FireIndex {
  static async create(path: string, firestore: admin.firestore.Firestore = admin.firestore()) {
    const qualifiedCollection = this.getQualifiedCollection(path);
    await firestore.doc(`fireIndex/${qualifiedCollection}`).set({count: admin.firestore.FieldValue.increment(1)}, {merge: true});
  }

  static async delete(path: string, firestore: admin.firestore.Firestore = admin.firestore()) {
    const qualifiedCollection = this.getQualifiedCollection(path);
    await firestore.doc(`fireIndex/${qualifiedCollection}`).set({count: admin.firestore.FieldValue.increment(-1)}, {merge: true});
  }

  static async count(path: string, firestore: admin.firestore.Firestore = admin.firestore()) {
    const qualifiedCollection = this.getQualifiedCollection(path);
    const snap = await firestore.doc(`fireIndex/${qualifiedCollection}`).get();
    return snap.exists ? snap.data().count : 0;
  }

  private static getQualifiedCollection(path: string) {
    const pathParts = path.split('/');
    return pathParts.slice(0, pathParts.length - 1).join('__');
  }
}
