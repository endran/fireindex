[![pipeline status](https://gitlab.com/endran/fireindex/badges/master/pipeline.svg)](https://gitlab.com/endran/fireindex/-/commits/master)
[![coverage report](https://gitlab.com/endran/fireindex/badges/master/coverage.svg)](https://gitlab.com/endran/fireindex/-/commits/master)
[![npm version](https://badge.fury.io/js/%40endra%2Ffireindex.svg)](https://badge.fury.io/js/%40endra%2Ffireindex)

# FireIndex

Utility functions to count the number of documents in a FireStore collection.

## Installation

```sh
npm install @endran/fireindex --save
```

or

```sh
yarn add @endran/fireindex
```

## Usage

In your `index.ts` file, add the following.

```typescript
exports.fireIndex = {
  create1: functions.firestore.document('{col1}/{doc1}').onCreate((snap) => FireIndex.create(snap.ref.path)),
  delete1: functions.firestore.document('{col1}/{doc1}').onDelete((snap) => FireIndex.delete(snap.ref.path)),
  create2: functions.firestore.document('{col1}/{doc1}/{col2}/{doc2}').onCreate((snap) => FireIndex.create(snap.ref.path)),
  delete2: functions.firestore.document('{col1}/{doc1}/{col2}/{doc2}').onDelete((snap) => FireIndex.delete(snap.ref.path)),
};
```

Add additional functions as your database has levels of nesting. Refrain from usage of `onUpdate`, since `onUpdate` fires much more often and does not change the number of documents in a collection.

FireIndex create a new collection called `fireIndex`, and for each collection in your database it will create a document.
When a document, ie: `someCol/someDoc/nestedCol/nestedDoc` is created, the document `fireIndex/someCol__someDoc__nestedCol` gets updated.
These documents only have 1 number property, called `count`.

## License

The MIT License.

```text
Copyright 2020 codecentric nl / David Hardy

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
```
